#include <iostream>
#include <opencv/highgui.h>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <time.h>
//#include "bagofword.h"

#include <vector>
#include <cmath>
#include "opencv2/core/core.hpp"
#include <opencv/cv.h>
#include "opencv2/nonfree/nonfree.hpp"
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/gpu/gpu.hpp>		

using namespace std;
using namespace cv;

int main()
{
	Mat img = imread("test.jpg");
	Mat gaussian_noise = img.clone();
	//imshow("test.jpg",gaussian_noise);
	randn(gaussian_noise,128,30);
	cv::add(img,gaussian_noise,img);
	//imshow("test.jpg",img);
	imwrite("test1.jpg",img);
	waitKey(0);
	return 0;
}