// step 3. SVM training

#include "creative_project.h"

void third_step()
{
	// TrainingDataset 불러오기
	FileStorage fs("traning_dataset.yml", FileStorage::READ);
	fs["vocabulary"] >> codeword_bowDescriptor_histogram;
	fs.release();
	// Dictionary	   불러오기
	FileStorage fs1("dictionary.yml", FileStorage::READ);
	fs1["vocabulary"] >> dictionary;
	fs1.release();

	_bowDE.setVocabulary(dictionary);			// ★ 설정

	/* SVM parameter 초기화 */
	param.svm_type = CvSVM::C_SVC;
	param.kernel_type = CvSVM::RBF;
	param.degree = 0;
	param.gamma = GAMMA;
	param.coef0 = COEF;
	param.C = C;
	param.nu = 0.12;
	param.p = 0.0;
	param.class_weights = NULL;
	param.term_crit.type = CV_TERMCRIT_ITER;
	param.term_crit.max_iter = 1000;
	param.term_crit.epsilon = 1e-6;

	// SVM training class 값 설정
	for(int i = 0; i < 80; i++)
	{
		if(i < 40)
			traningClasses.at<float>(i) = BUILDING;
		else if(i < 80)
			traningClasses.at<float>(i) = CAR;
	}

	CvSVM svm(codeword_bowDescriptor_histogram, traningClasses, Mat(), Mat(), param);	// SVM training

	svm.save("svm.yml",0);
}