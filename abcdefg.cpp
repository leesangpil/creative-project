#include <iostream>
#include <opencv/highgui.h>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <time.h>
#include <windows.h>
using namespace cv;
using namespace std;

#define CODEWORD_START      1               // 코드워드를 만들 사진의 시작 인덱스
#define CODEWORD_END      140               // 코드워드를 만들 사진의 마지막 인덱스
#define DICTIONARY_SIZE_K   50000            // 코드워드의 갯수 (K-means-clustering의 K값)
#define INPUT_IMAGE_INDEX   6               // 비교할 이미지


#define CATEGORY_SIZE  2
//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
#define DICTIONARY_BUILD   2              // 1 설정 = 학습 , else 설정 = 추출



int main()
{
#if DICTIONARY_BUILD == 0                  // Step 1 - CodeBook Generation
	LARGE_INTEGER liCounter1, liCounter2, liFrequency;
	QueryPerformanceFrequency(&liFrequency);

	/* variable */
	Mat featuresUnclustered;               // 전체 이미지의 특징점 descripor들을 한곳에 저장할 변수


	/* main loop */
	char dataset[2][20]={"building","car"};
	for(int j=0; j<CATEGORY_SIZE; j++){
		for(int f = CODEWORD_START; f <= CODEWORD_END; f++)
		{
			if(j == 1 && f >80) break;
			char * filename = new char[100];         // 저장경로를 포함한 파일 이름
			sprintf(filename,"C:\\testimages\\last\\%s\\%s_%d.jpg",dataset[j],dataset[j],f);// 불러올 파일 이미지의 경로값 변경
			Mat Full_Panorama_img = imread(filename, 1);
			if(!Full_Panorama_img.data){
				printf("이미지 적재 실패 %d %d\n",j,f);
				return -1;
			}
			SiftDescriptorExtractor detector;         // SIFT 알고리즘(descripor 추출자)
			Mat descriptor;                        // 현재 이미지에 특징점 descripor를 저장할 변수

			vector<KeyPoint> input_image_keypoints;
			detector.detect(Full_Panorama_img, input_image_keypoints); // 사진에서 특징점을 뽑아낸다
			detector.compute(Full_Panorama_img, input_image_keypoints, descriptor);                  // 각 특징점에서 descripor 추출

			featuresUnclustered.push_back(descriptor);                     // 모든 descripor를 저장
		}
		printf("featuresUnclustered size : %d\n",featuresUnclustered.rows);
	}


	// TermCriteria 클래스는 반복적인 연산을 수행하는 알고리즘의 종료기준을 잡아주는 클래스
	// TermCriteria(int type, int maxCount, double epsilon) 로 구성되어 있다.
	// type : 종료기준에 대한 타입을 지정함
	// maxCount : 최대 반복 횟수
	// epsilon : 종료 기준 설정
	TermCriteria tc(CV_TERMCRIT_ITER, 100, 0.001);

	// BOWKMeansTrainer(int clustercount, const TermCriteria, int attempts, int flags)
	// clustercount : 몇개의 클러스터로 군집화를 시킬 것인가?
	// TermCriteria : 위쪽에 설명. tc로 사용
	// attempts : 시도횟수
	// flags : KMEANS_PP_CENTERS 로 사용
	QueryPerformanceCounter(&liCounter1);   // start
	BOWKMeansTrainer bowTrainer(DICTIONARY_SIZE_K, tc, 1, KMEANS_PP_CENTERS);
	Mat dictionary = bowTrainer.cluster(featuresUnclustered);      // 특징벡터들을 K-means-clustering 으로 군집화 시킨다
	QueryPerformanceCounter(&liCounter2);   // end

	/* CodeBook Storage */
	char * filename = new char[100];         // 저장경로를 포함한 파일 이름

	sprintf(filename,"last_dictionary_%d.yml",DICTIONARY_SIZE_K);// 불러올 파일 이미지의 경로값 변경

	FileStorage fs(filename, FileStorage::WRITE);
	fs << "vocabulary" << dictionary;
	fs.release();
	printf("end\n");
	printf("Time : %f\n", (double)(liCounter2.QuadPart - liCounter1.QuadPart) / (double)liFrequency.QuadPart);
#elif DICTIONARY_BUILD==1                                             // Step 2 - image data 학습 및 분류(SVM)
	/* variable */
	for(int iiii = 70000; iiii<=100000; iiii+=10000){
		clock_t start_time, end_time;

		start_time = clock();

		char * filename = new char[100];                           // 저장경로를 포함한 분류할 파일 이름 (input image)
		Mat dictionary;                                          // K-means-clustering에 의해 군집화된 특징점들(Codeword)
		Mat codeword_bowDescriptor_histogram;                        // 히스토그램 저장할 변수

		vector<KeyPoint> keypoints;                                 // 특징점
		Ptr<DescriptorMatcher> matcher(new FlannBasedMatcher);            // nearest neighbor matcher 추출자
		Ptr<FeatureDetector> detector(new SiftFeatureDetector());         // SIFT알고리즘(특징점 추출자)
		Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor);   // SIFT 알고리즘(descripor 추출자)

		/* 코드 북 불러오기 */
		char * dictionary_filename = new char[100];         // 저장경로를 포함한 파일 이름
		sprintf(dictionary_filename,"last_dictionary_%d.yml",iiii);
		FileStorage fs(dictionary_filename, FileStorage::READ);
		fs["vocabulary"] >> dictionary;
		fs.release();

		// Bag-of-Feature(Word) descriptor 생성
		BOWImgDescriptorExtractor bowDE(extractor,matcher);
		bowDE.setVocabulary(dictionary);                              // 1단계에서 만든 CodeWord들로 단어들을 설정함

		char dataset[2][20]={"building","car"};
		for(int j=0; j<CATEGORY_SIZE; j++){
			for(int f = CODEWORD_START; f <= CODEWORD_END; f++)
			{
				if(j == 1 && f >80) break;
				char * filename = new char[100];         // 저장경로를 포함한 파일 이름
				sprintf(filename,"C:\\testimages\\last\\%s\\%s_%d.jpg",dataset[j],dataset[j],f);// 불러올 파일 이미지의 경로값 변경
				Mat Full_Panorama_img = imread(filename, 1);

				if(!Full_Panorama_img.data){
					printf("이미지 적재 실패\n");
					return -1;
				}

				SiftDescriptorExtractor detector;         // SIFT 알고리즘(descripor 추출자)

				vector<KeyPoint> input_image_keypoints;
				detector.detect(Full_Panorama_img, input_image_keypoints); // 사진에서 특징점을 뽑아낸다               // 각 특징점에서 descripor 추출
				Mat codeword_bowDescriptor_histogram_all;   // 히스토그램 모아서 저장할 변수
				bowDE.compute(Full_Panorama_img, input_image_keypoints, codeword_bowDescriptor_histogram_all);
				if(codeword_bowDescriptor_histogram_all.rows == 0){
					printf("%d %d\n",j,f);
				}
				codeword_bowDescriptor_histogram.push_back(codeword_bowDescriptor_histogram_all);

			}
		}
		char * training_filename = new char[100];         // 저장경로를 포함한 파일 이름
		sprintf(training_filename,"last_tranining_dataset_%d.yml",iiii);
		FileStorage fs2(training_filename, FileStorage::WRITE);
		fs2 << "vocabulary" << codeword_bowDescriptor_histogram;
		fs2.release();
		end_time = clock();
		printf("Time : %f\n",((double)(end_time-start_time))/CLOCKS_PER_SEC);
	}
#elif DICTIONARY_BUILD==2   
	//supervised learning (class 생성 및 구분)

	//시간측정

	//clock_t start_time1, end_time1;
	//start_time1 = clock();
	////////////read start
	for(int iiii = 90000; iiii<=90000; iiii+=10000){
		char * filename = new char[100];
		Mat dictionary;
		Mat codeword_bowDescriptor_histogram;
		Ptr<DescriptorMatcher> matcher(new FlannBasedMatcher);            // nearest neighbor matcher 추출자
		Ptr<FeatureDetector> detector(new SiftFeatureDetector());         // SIFT알고리즘(특징점 추출자)
		Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor);   // SIFT 알고리즘(descripor 추출자)

		char * training_filename = new char[100];         // 저장경로를 포함한 파일 이름
		sprintf(training_filename,"last_tranining_dataset_%d.yml",iiii);

		FileStorage fs(training_filename, FileStorage::READ);
		fs["vocabulary"] >> codeword_bowDescriptor_histogram;
		fs.release();

		char * dictionary_filename = new char[100];         // 저장경로를 포함한 파일 이름
		sprintf(dictionary_filename,"last_dictionary_%d.yml",iiii);

		FileStorage fs1(dictionary_filename, FileStorage::READ);
		fs1["vocabulary"] >> dictionary;
		fs1.release();
		BOWImgDescriptorExtractor bowDE(extractor,matcher);
		bowDE.setVocabulary(dictionary);   
		/////////////read end

		/* SVM parameter */
		////svm traning start

		/* SVM parameter */
		////svm traning start
		double GAMMA = 4;
		double COEF = 0;
		double C = 500;
		CvSVMParams param = CvSVMParams();
		param.svm_type = CvSVM::C_SVC;
		param.kernel_type = CvSVM::RBF;
		param.degree = 0;
		param.gamma = GAMMA;
		param.coef0 = COEF;
		param.C = C;
		param.nu = 0.12;
		param.p = 0.0;
		param.class_weights = NULL;
		//param.term_crit.type = CV_TERMCRIT_ITER + CV_TERMCRIT_EPS;
		param.term_crit.type = CV_TERMCRIT_ITER;
		param.term_crit.max_iter = 1000;
		param.term_crit.epsilon = 1e-6;

		Mat traningClasses(220, 1, CV_32FC1);
		for(int i = 0; i < traningClasses.rows; i++)    //1 : 건물 / 2 : 차 /
		{
			if(i<140)
				traningClasses.at<float>(i) = 1;
			else if(i<traningClasses.rows)
				traningClasses.at<float>(i) = 2;
		}
		// SVM trainer
		CvSVM svm(codeword_bowDescriptor_histogram, traningClasses, Mat(), Mat(), param);
		////svm traning end
		double time = 0;
		for(int a = 81; a < 101; a++){
			LARGE_INTEGER liCounter1, liCounter2, liFrequency;
			QueryPerformanceFrequency(&liFrequency);
			QueryPerformanceCounter(&liCounter1);   // start
			// input image -> 구분할 이미지 Descriptor 생성
			Mat input_image_bowDescriptor;
			// 파노라마 이미지 적재
			char * panorama_filename = new char[100];
			if(a < 10){
				sprintf(panorama_filename,"pano00%d.jpg",a);
			}else if(a < 100){
				sprintf(panorama_filename,"pano0%d.jpg",a);
			}else{
				sprintf(panorama_filename,"pano%d.jpg",a);
			}



			sprintf(filename,"C:\\testimages\\image\\panorama1\\%s",panorama_filename);   // 4096 * 2048
			Mat Full_Panorama_img = imread(filename, 1);
			Mat Binary_Panorama_img = imread(filename, 0);
			if(!Full_Panorama_img.data){
				printf("이미지 적재 실패\n");
				return -1;
			}
			int width = 256, height = 256; //이미지 분할 갯수 정하기 16 x 16 / 10 x 10 / ... 
			Rect rect[4]; // 파노라마 이미지를 분할하기 위한 Rect 선언
			int sub_panorama_object_cnt[4096][2048];
			for(int i = 0; i < 4096; i++){
				for(int j = 0; j < 2048; j++){
					sub_panorama_object_cnt[i][j] = 0;
				}
			}
			printf("Setting Done\n");
			// 특징점이 없는것들은 keypoint가 안잡혀 codeword 및 히스토그램이 안뽑힌다.
			vector<vector<KeyPoint>> exist_image_keypoints; //존재하는 키포인트들의 벡터
			for(int i = 0; i <= (2048-height); i+=64)
			{
				for(int j = 0; j <= (4096-width); j+=64)
				{
					Rect rect(j, i, width, height);
					vector<KeyPoint> input_image_keypoints;
					Mat sub_panorama = Full_Panorama_img(rect);
					detector->detect(sub_panorama, input_image_keypoints); // 사진에서 특징점을 뽑아낸다
					Mat input_image_bowDescriptor_all;
					bowDE.compute(sub_panorama, input_image_keypoints, input_image_bowDescriptor_all);
					if(input_image_bowDescriptor_all.rows == 0){ // codeword가 안뽑히면 체크해주고 다음 작업을 수행하지않고 
						//exist_keypoint_addr[subpana_arr_Number]=0;

					}else{
						//exist_keypoint_addr[subpana_arr_Number]=1;
						int object_num = svm.predict(input_image_bowDescriptor_all);
						for(int h = i;  h<(i+height); h++){
							for(int w = j; w<(j+width); w++){
								if(object_num == 1){
									sub_panorama_object_cnt[w][h] += 1;
								}else if(object_num == 2){
									sub_panorama_object_cnt[w][h] -= 1;
								}
							}
						}

					}

				}
			}

			for(int i = 0; i < 4096; i++)
			{
				for(int j = 0; j < 2048; j++)
				{
					if(sub_panorama_object_cnt[i][j] > 0){
						//Full_Panorama_img.at<Vec3b>(j,i)[0] = 255;
						Full_Panorama_img.at<Vec3b>(j,i)[1] = 255;
						//Full_Panorama_img.at<Vec3b>(j,i)[2] = 255;
						Binary_Panorama_img.at<uchar>(j,i) = 0;
					}else{
						//Full_Panorama_img.at<Vec3b>(j,i)[0] = 0;
						//Full_Panorama_img.at<Vec3b>(j,i)[1] = 0;
						Full_Panorama_img.at<Vec3b>(j,i)[2] = 255;
						Binary_Panorama_img.at<uchar>(j,i) = 255;
					}
				}

			}
			char filename2[100];
			sprintf(filename2,"C:\\testimages\\k_%d_color_%d_64.jpg",iiii,a);
			imwrite(filename2,Full_Panorama_img); 
			sprintf(filename2,"C:\\testimages\\k_%d_binary_%d_64.jpg",iiii,a);
			imwrite(filename2,Binary_Panorama_img); 
			QueryPerformanceCounter(&liCounter2);   // end
			//printf("Time : %f\n", (double)(liCounter2.QuadPart - liCounter1.QuadPart) / (double)liFrequency.QuadPart);
			time+=((double)(liCounter2.QuadPart - liCounter1.QuadPart) / (double)liFrequency.QuadPart);
		}
		Mat save_file(10,10,1);
		char* file_name_111 = new char[100];
		sprintf(file_name_111,"C:\\testimages\\%lf.jpg",time/20);
		imwrite(file_name_111, save_file);
		printf("%d 평균걸린 시간 : %lf\n",iiii,time/20);
		printf("%d 총 걸린 시간 : %lf\n",iiii,time);
	}
#endif
	cout << endl << "done" << endl;

	return 0;
}