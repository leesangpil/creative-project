#ifndef _creative_project_h
#define	_creative_project_h

#include <iostream>
#include <opencv/highgui.h>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <time.h>
//#include "bagofword.h"

#include <vector>
#include <cmath>
#include "opencv2/core/core.hpp"
#include <opencv/cv.h>
#include "opencv2/nonfree/nonfree.hpp"
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/gpu/gpu.hpp>					// CUDA

using namespace cv;
using namespace std;


#define CODEWORD_START		1					// 코드워드를 만들 사진의 시작 인덱스
#define CODEWORD_END		40					// 코드워드를 만들 사진의 마지막 인덱스
#define DICTIONARY_SIZE_K	16000				// 코드워드의 갯수 (K-means-clustering의 K값)
#define INPUT_IMAGE_INDEX	6					// 비교할 이미지
#define CATEGORY_SIZE  2						// 이미지 분류 카테고리 수
#define BUILDING 1
#define CAR	2


/* gamma, c, coef */
const double GAMMA = 4;
const double C = 48;
const double COEF = 0;

static CvSVMParams param = CvSVMParams();									// SVM parameter 객체(전역 변수)
static Mat traningClasses(80, 1, CV_32FC1);									// SVM training class 선언(전역 변수)

static Mat dictionary;														// 불러올 dictionary 파일 (전역변수)
static Mat codeword_bowDescriptor_histogram;								// 불러올 TrainingDataset 파일 (전역변수)

static Ptr<DescriptorMatcher> _matcher(new FlannBasedMatcher);				// nearest neighbor matcher 추출자 (전역변수)
static Ptr<FeatureDetector> _detector(new SiftFeatureDetector());			// SIFT 특징점 추출자 (전역변수)
static Ptr<DescriptorExtractor> _extractor(new SiftDescriptorExtractor);	// SIFT descripor 추출자 (전역변수)

static BOWImgDescriptorExtractor _bowDE(_extractor,_matcher);				// ★ (전역변수)


void first_step();								// step 1. CodeBook Generation
void second_step();								// step 2. CodeBook's Histogram extract (SVM training Data-Set extract)
void third_step();								// step 3. SVM training
void fourth_step();								// step 4. Image Labeling
void fifth_step();								// step 5. Euclidian matching

// -------------------------------------------------------------------------------------------------하는 중
const int DIM_VECTOR = 128;    // 128
const double THRESHOLD = 0.27;  // 0.3

double euclidDistance(float* vec1, float* vec2, int length);
int nearestNeighbor(float* vec, int laplacian, CvSeq* keypoints, CvSeq* descriptors);
void findPairs(CvSeq* keypoints1, CvSeq* descriptors1, CvSeq* keypoints2, CvSeq* descriptors2, vector<int>& ptpairs);

#endif